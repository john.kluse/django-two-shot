from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
      "name",
      "owner",
      "id",
    ]

@admin.register(Account)
class AccountCategoryAdmin(admin.ModelAdmin):
    list_display = [
      "name",
      "number",
      "owner",
    ]

@admin.register(Receipt)
class ReceiptCategoryAdmin(admin.ModelAdmin):
    list_display = [
      "vendor",
      "total",
      "tax",
      "date",
      "purchaser",
      "category",
      "account"
    ]
